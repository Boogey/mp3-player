package search
{
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.FileListEvent;
	import flash.events.IEventDispatcher;
	import flash.filesystem.File;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.net.FileReferenceList;
	import mx.collections.ArrayList;
	
	import flashx.textLayout.factory.TruncationOptions;
	
	public class FileSearch extends EventDispatcher
	{
		// This Array contains valid extensions
		private var fileTypes:Array = new Array();
		// This Array contains file list
		[Bindable]
		public var list:ArrayList;
		// This Array contains all subfolders not yet searched
		public var queue:Array = new Array();
		
		public function FileSearch(target:IEventDispatcher=null)
		{
			super(target);
			fileTypes.push("mp3");
			list = new ArrayList();
		}
		
		// Check whether or not a file is a valid song
		public function isValidType(type:String):Boolean
		{
			if (fileTypes.indexOf(type) > -1)
				return true;
			else
				return false;
		}
		
		// Search for all songs in a directory
		// TODO : add io handler
		public function search(root:File):void
		{
			if (root.isDirectory) {
				root.addEventListener(FileListEvent.DIRECTORY_LISTING, _directoryListingHandler);
				//root.addEventListener(ErrorEvent.ERROR, _ioErrorHandler);
				root.getDirectoryListingAsync();
			}
		}
		
		// Add all songs to an Array once the search is complete
		protected function _directoryListingHandler(event:FileListEvent):void {
			for (var i:uint = 0; i < event.files.length; i++) {
				if (isValidType(event.files[i].extension))
					list.addItem(event.files[i]);
				else if (event.files[i].isDirectory)
					queue.push(event.files[i]);
			}
			
			if (queue.length > 0) {
				search(queue.pop());
			}
		}
	}
}