package sound
{
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;

	public class CustomSound extends Sound
	{
		public function CustomSound(path: String)
		{
			super(new URLRequest(path));
		}
		
		public function stop ():void {
			
		}
	}
}