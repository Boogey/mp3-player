package com.fusiox.ui
{
	import mx.controls.Image;
	import flash.utils.ByteArray;
	import flash.system.LoaderContext;
	import flash.display.Loader;
	import flash.display.DisplayObject;
	import flash.events.Event;

	public class Image extends mx.controls.Image
	{
		private var _loader:Loader = new Loader();
		
		public function Image():void {
		}
		
		public function clear():void
		{
			_loader.alpha = 0;
		}
		
		public function loadBytes(bytes:ByteArray, context:LoaderContext = null):void {
			_loader.loadBytes(bytes);
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onBytesLoaded);
		}
		
		private function onBytesLoaded( e:Event ):void {
			
			_loader.width = 430;
			_loader.height = 430;
			_loader.alpha = 1;
			addChild(_loader);
		}
	}
}